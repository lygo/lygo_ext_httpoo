module bitbucket.org/lygo/lygo_ext_httpoo

go 1.16

require (
	4d63.com/gochecknoglobals v0.0.0-20210416044342-fb0abda3d9aa // indirect
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_email v0.1.6
	bitbucket.org/lygo/lygo_events v0.1.10
	bitbucket.org/lygo/lygo_ext_auth0 v0.1.22
	bitbucket.org/lygo/lygo_ext_dbal v0.1.31
	bitbucket.org/lygo/lygo_ext_http v0.1.20
	bitbucket.org/lygo/lygo_ext_logs v0.1.9
	bitbucket.org/lygo/lygo_ext_sms v0.1.5
	github.com/BurntSushi/toml v0.4.0 // indirect
	github.com/Djarvur/go-err113 v0.1.0 // indirect
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/cbroglie/mustache v1.2.2
	github.com/daixiang0/gci v0.2.9 // indirect
	github.com/fasthttp/websocket v1.4.3 // indirect
	github.com/go-critic/go-critic v0.5.7 // indirect
	github.com/gofiber/fiber/v2 v2.16.0
	github.com/gofiber/websocket/v2 v2.0.7 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	github.com/golangci/golangci-lint v1.41.1 // indirect
	github.com/google/addlicense v0.0.0-20200817051935-6f4cd4aacc89 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gordonklaus/ineffassign v0.0.0-20210729092907-69aca2aeecd0 // indirect
	github.com/gostaticanalysis/analysisutil v0.7.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/ldez/gomoddirectives v0.2.2 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mgechev/revive v1.0.9 // indirect
	github.com/nishanths/exhaustive v0.2.3 // indirect
	github.com/polyfloyd/go-errorlint v0.0.0-20210722154253-910bb7978349 // indirect
	github.com/prometheus/common v0.30.0 // indirect
	github.com/prometheus/procfs v0.7.1 // indirect
	github.com/quasilyte/regex/syntax v0.0.0-20200805063351-8f842688393c // indirect
	github.com/savsgio/gotils v0.0.0-20210617111740-97865ed5a873 // indirect
	github.com/securego/gosec/v2 v2.8.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/cast v1.4.0 // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/tetafro/godot v1.4.8 // indirect
	github.com/timakin/bodyclose v0.0.0-20210704033933-f49887972144 // indirect
	github.com/tomarrell/wrapcheck/v2 v2.3.0 // indirect
	github.com/uudashr/gocognit v1.0.5 // indirect
	github.com/valyala/fasthttp v1.28.0 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/tools v0.1.5 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	mvdan.cc/unparam v0.0.0-20210701114405-894c3c7ee6a6 // indirect
	sourcegraph.com/sqs/pbtypes v1.0.0 // indirect
)
