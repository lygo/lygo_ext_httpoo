import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import constants from "../app-commons/constants";

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={(props) =>
            !!localStorage.getItem(constants.FLD_TOKEN_ACCESS) ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{pathname: '/login', state: {from: props.location}}}
                />
            )
        }
    />
);
