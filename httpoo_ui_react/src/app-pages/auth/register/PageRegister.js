import {Component, Fragment} from "react";
import Title from "antd/es/typography/Title";
import {Col, Row} from "antd";
import CompRegister from "../../../app-components/register/CompRegister";
import random from "../../../libs/random";
import {EventEmitterSingleton} from "../../../libs/events/EventEmitter";
import constants from "../../../app-commons/constants";
import {withRouter} from "react-router-dom";

class PageRegister extends Component {

    history;
    state = {
        loading: false
    };

    constructor(props) {
        super(props);
        this.key = random.id();
        this.history = props.history;
    }

    componentDidMount() {
        EventEmitterSingleton.on(this, constants.EventLoginStart, () => {
            this.setState({loading: true});
        });
        EventEmitterSingleton.on(this, constants.EventLoginSuccess, () => {
            this.history.push("/home");
        });
        EventEmitterSingleton.on(this, constants.EventLoginError, (err) => {
            this.setState({loading: false});
        });
    }

    componentWillUnmount() {
        EventEmitterSingleton.off(this);
    }

    render() {
        const loading = !!this.state.loading;
        return (
            <Fragment>
                <div style={{textAlign: "center"}}>
                    <Title>LOGIN PAGE</Title>
                    <Row justify="center">
                        <Col span={12}>
                            <CompRegister loading={loading}/>
                        </Col>
                    </Row>
                </div>

            </Fragment>

        );
    }
}

export default withRouter(PageRegister);