import {Component} from "react";
import logo from "../../logo.svg";
import {Button, message} from "antd";
import {EventEmitterSingleton} from "../../libs/events/EventEmitter";
import constants from "../../app-commons/constants";
import random from "../../libs/random";
import services from "../../app-commons/services";
import fmt from "../../libs/fmt";

class PageDashboard extends Component {

    history;

    constructor(props) {
        super(props);
        this.key = random.id();
        this.history = props.history;
    }

    componentDidMount() {
        console.log("ENTERING PROTECTED AREA!");
        this.doProtectedQuery();
    }

    doExit() {
        EventEmitterSingleton.emit(constants.EventLoginExit);
        this.history.push(constants.RouteLogin);
    }

    doProtectedQuery() {
        services.user((err, user) => {
            if (!!err) {
                message.error(fmt.template("Error: ({{code}}) {{message}}", err));
            } else if (!!user) {
                message.info("Welcome, " + user.firstname);
            }
        })
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        Welcome to a protected Dashboard.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                    <Button type="primary" onClick={this.doExit.bind(this)}>
                        Logout
                    </Button>
                    <Button onClick={this.doProtectedQuery.bind(this)}>
                        Test Forbidden Query
                    </Button>

                </header>
            </div>
        );
    }
}

export default PageDashboard