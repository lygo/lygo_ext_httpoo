import {Component, Fragment} from "react";
import {Button, Checkbox, Form, Input, message, Skeleton} from 'antd';
import {EventEmitterSingleton} from "../../libs/events/EventEmitter";
import constants from "../../app-commons/constants";
import services from "../../app-commons/services";
import fmt from "../../libs/fmt";

class CompLogin extends Component {

    state = {error: ""};

    constructor(props) {
        super(props);
        this.fields = {
            username: null,
            password: null,
            rememberMe: null,
        }
    }

    setRefUsername(element) {
        this.fields.username = element;
        if (!!this.fields.username) {
            this.fields.username.value = "";
        }
    }

    setRefPassword(element) {
        this.fields.password = element;
        if (!!this.fields.password) {
            this.fields.password.value = "";
        }
    }

    setRefRememberMe(element) {
        this.fields.rememberMe = element;
        if (!!this.fields.rememberMe) {
            this.fields.rememberMe.value = true;
        }
    }

    // JUST FOR TESTING A FORBIDDEN ACCESS
    doProtectedQuery(){
        services.user((err, user)=>{
            if (!!err){
                message.error(fmt.template("Error: ({{code}}) {{message}}", err));
            } else if (!!user){
                message.info("Welcome, " + user.firstname);
            }
        })
    }

    onSubmit() {
        if (!!this) {
            if (!!this.fields.username && !!this.fields.password) {
                const username = this.fields.username.state.value;
                const password = this.fields.password.state.value;
                const remember = this.fields.rememberMe.props.checked;
                console.debug("onSubmit", username, password, remember);
                if (!!username && !!password) {
                    // emit start
                    EventEmitterSingleton.emit(constants.EventLoginStart);
                    // invoke service
                    services.login(username, password, (error, response) => {
                        if (!!error) {
                            message.error(fmt.template("Error: ({{code}}) {{message}}", error));
                            // emit error
                            EventEmitterSingleton.emit(constants.EventLoginError, error);
                        } else if (!!response){
                            // got a response containing user and tokens.
                            EventEmitterSingleton.emit(constants.EventLoginSuccess, response);
                        }
                    });
                } else {
                    // missing username or password
                    message.error("Missing username or password.");
                }
            } else {
                // missing binding
                message.error("System Error.");
            }
        }
    }

    render() {
        const loading = !!this.props.loading;

        return (
            <Fragment>
                {loading
                    ? <Skeleton active/>
                    : <Form
                        name="basic"
                        labelCol={{span: 8}}
                        wrapperCol={{span: 16}}
                        initialValues={{remember: true}}
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[{required: true, message: 'Please input your username!'}]}
                        >
                            <Input ref={this.setRefUsername.bind(this)}/>
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[{required: true, message: 'Please input your password!'}]}
                        >
                            <Input.Password ref={this.setRefPassword.bind(this)}/>
                        </Form.Item>

                        <Form.Item name="remember" valuePropName="checked" wrapperCol={{offset: 8, span: 16}}>
                            <Checkbox ref={this.setRefRememberMe.bind(this)}>Remember me</Checkbox>
                        </Form.Item>

                        <Form.Item wrapperCol={{offset: 8, span: 16}}>
                            <Button type="primary" onClick={this.onSubmit.bind(this)}>
                                Submit
                            </Button>
                        </Form.Item>
                        <Form.Item wrapperCol={{offset: 8, span: 16}}>
                            <Button onClick={this.doProtectedQuery.bind(this)}>
                                Test Forbidden Query
                            </Button>
                        </Form.Item>
                    </Form>
                }

            </Fragment>
        );
    }
}

export default CompLogin