import Events, {Listener} from "./Events";
import {Dictionary} from "../collections/Dictionary";

/**
 * Class that emit events with a scope.
 */
class EventEmitter {

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    private readonly _listeners: Dictionary<Events>;

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    constructor() {
        this._listeners = new Dictionary<Events>();
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public on(scope: any, eventName: string, listener: Listener): void {
        if (!!scope) {
            let key: string = EventEmitter.key(scope);
            if (!this._listeners.containsKey(key)) {
                this._listeners.put(key, new Events());
            }
            this._listeners.get(key).on(eventName, listener.bind(scope));
        }
    }

    public once(scope: any, eventName: string, listener: Listener): void {
        if (!!scope) {
            let key: string = EventEmitter.key(scope);
            if (!this._listeners.containsKey(key)) {
                this._listeners.put(key, new Events());
            }
            this._listeners.get(key).once(eventName, listener.bind(scope));
        }
    }

    public off(scope: any, eventName?: string): void {
        if (!!scope) {
            let key: string = EventEmitter.key(scope);
            if (this._listeners.containsKey(key)) {
                this._listeners.get(key).off(eventName);
            }
        }
    }

    public clear(): void {
        if (!!this._listeners) {
            let keys: string[] = this._listeners.keys();
            for (let key of keys) {
                if (this._listeners.containsKey(key)) {
                    this._listeners.get(key).clear();
                }
            }
        }
    }

    public emit(eventName: string, ...args: any[]): void {
        if (!!this._listeners) {
            let keys: string[] = this._listeners.keys();
            for (let key of keys) {
                if (this._listeners.containsKey(key)) {
                    this._listeners.get(key).emit(eventName, ...args);
                }
            }
        }
    }

    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    private static key(scope: any): string {
        try {
            let response = scope["uid"] || scope["key"] || scope["id"];
            if (!response) {
                console.warn("EventEmitter.key()", "BINDING EVENT ON DEFAULT KEY!");
                response = '_default';
            }
            console.debug("KEY", response, scope);
            return response;
        } catch (err) {
            console.warn("EventEmitter.key()", "BINDING EVENT ON DEFAULT KEY!");
            return '_default';
        }
    }

    // ------------------------------------------------------------------------
    //                      S I N G L E T O N
    // ------------------------------------------------------------------------

    private static __instance: EventEmitter;

    public static instance(): EventEmitter {
        if (null == EventEmitter.__instance) {
            EventEmitter.__instance = new EventEmitter();
        }
        return EventEmitter.__instance;
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

const EventEmitterSingleton: EventEmitter = EventEmitter.instance();
export {EventEmitterSingleton, EventEmitter};