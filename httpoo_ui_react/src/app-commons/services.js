import constants from "./constants";

const services = {};

services.login = function (username, password, callback) {
    const url = "/api/httpoo/auth/sign-in";
    const params = {
        email: username,
        password: password,
    }
    doRequest("post", url, params, callback);
}

services.register = function (email, password, firstname, lastname, privacy_policy_consent, callback) {
    const url = "/api/httpoo/auth/sign-up";
    const params = {
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname,
        privacy_policy_consent: privacy_policy_consent
    }
    doRequest("post", url, params, callback);
}

// protected method
services.user = function (callback) {
    // console.log("services.user");
    const url = "/api/httpoo/auth/user";
    doRequest("get", url, null, callback);
}


services.refresh = function (callback) {
    const url = "/api/httpoo/auth/refresh-token";
    const params = {
        token: localStorage[constants.FLD_TOKEN_REFRESH]
    }
    constants.API().post(url, params)
        .catch(error => {
            console.error("SYSTEM ERROR", url, getError(error))
            callback(getError(error), null);
        })
        .then(value => {
            value = value || {};
            const data = value.data || {};
            if (!!data.error) {
                console.error("AUTH ERROR", url, data);
                callback(getError(data.error), null);
            } else {
                callback(null, data.response);
            }
        });
}

function getError(error) {
    // console.debug(JSON.stringify(error));
    if (!!error) {
        const data = !!error.response ? error.response : error;
        const message = data.message || data;
        const status = data.status || 500;
        if (!!message.data && !!message.data.response) {
            return {
                "code": message.data.response.code,
                "message": message.data.error || message.data.response.message
            }
        }
        return {
            "code": status,
            "message": message
        }
    }
    return undefined;
}

function doRequest(method, url, params, callback) {
    caller(method, url, params).catch(err => {
        const error = getError(err);
        // check if error is a "token_expired" error
        validateError(error, (validatedError) => {
            if (!!validatedError) {
                console.error("SYSTEM ERROR", url, validatedError)
                callback(validatedError, null);
            } else {
                // retry call
                caller(method, url, params).catch(err => {
                    const error = getError(err);
                    console.error("SYSTEM ERROR", url, error)
                    callback(error, null);
                }).then(value => {
                    value = value || {};
                    const data = value.data || {};
                    if (!!data.error) {
                        console.error("AUTH ERROR", url, data);
                        callback(getError(data.error), null);
                    } else {
                        callback(null, data.response);
                    }
                });
            }
        });
    }).then(value => {
        value = value || {};
        const data = value.data || {};
        if (!!data.error) {
            console.error("AUTH ERROR", url, data);
            callback(getError(data.error), null);
        } else {
            callback(null, data.response);
        }
    });
}

function caller(method, url, params) {
    if ("get" === method.toLowerCase()) {
        if (!!params) {
            return constants.API().get(url, params);
        }
        return constants.API().get(url)
    } else if ("post" === method.toLowerCase()) {
        if (!!params) {
            return constants.API().post(url, params);
        }
        return constants.API().post(url)
    } else if ("delete" === method.toLowerCase()) {
        if (!!params) {
            return constants.API().delete(url, params);
        }
        return constants.API().delete(url)
    } else if ("put" === method.toLowerCase()) {
        if (!!params) {
            return constants.API().put(url, params);
        }
        return constants.API().put(url)
    }
    // fallback
    return constants.API().get(url);
}

function validateError(error, callback) {
    const code = !!error ? error.code : 0;
    const message = !!error ? error.message : "";
    console.debug("validateError", code, message);
    if (code === 403 && message === "access_token_expired") {
        console.debug("validateError", "REFRESHING");
        // token expired, try to refresh
        services.refresh((err, response) => {
            const accessToken = !!response ? response["access_token"] || "" : "";
            if (!!err || !accessToken) {
                console.debug("validateError", "REFRESH ERROR");
                callback(getError(err));
            } else {
                console.debug("validateError", accessToken);
                localStorage[constants.FLD_TOKEN_ACCESS] = accessToken;
                callback();
            }
        });
    } else {
        callback(error);
    }
}

export default services;