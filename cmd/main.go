package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_commons"
	"flag"
	"log"
	"os"
)

const AppName = "HTTPoo"

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION '%s' ERROR: %s", AppName, r)
			log.Fatalf(message)
		}
	}()

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirWork := cmdRun.String("dir_work", lygo_paths.Absolute("./httpoo"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", httpoo_commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	quit := cmdRun.String("s", "stop", "Quit Command: Write a command (ex: 'stop') to enable stop mode")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		default:
			panic("Command not supported: " + cmd)
		}
	} else{
		panic("Missing command. i.e. 'run'")
	}

	initialize(dirWork, mode)

	app, err := httpoo.LaunchHTTPoo(*mode, *quit)
	if nil == err {
		err = app.Start()
		if nil != err {
			log.Panicf("Error starting %s: %s", httpoo_commons.AppName, err.Error())
		} else {
			// app is running
			app.Join()
		}
	} else {
		log.Panicf("Error launching %s: %s", httpoo_commons.AppName, err.Error())
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func initialize(dirWork *string, mode *string) {
	lygo_paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(*dirWork)
}
