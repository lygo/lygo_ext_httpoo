package httpoo_ctrl

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
)

/**
Download templates from GitHub or Bitbucket
*/

const (
	repoRoot = "https://bitbucket.org/lygo/lygo_ext_httpoo/raw/master/"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func DownloadTemplates(targetDir string) ([]string, []error) {
	// download
	session := lygo_io.NewDownloadSession(templateActions(targetDir))
	return session.DownloadAll(false)
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func templateActions(dirTemplates string) []*lygo_io.DownloaderAction {
	response := make([]*lygo_io.DownloaderAction, 0)

	// email
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/password-reset.html.htm", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/password-reset.text.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/password-reset.subject.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/verify.html.htm", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/verify.text.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/verify.subject.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/welcome.html.htm", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/welcome.text.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/email/welcome.subject.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "email")))

	// sms
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/sms/verify.text.txt", "",
		lygo_paths.ConcatDir(dirTemplates, "sms")))

	// html
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/html/error.html", "",
		lygo_paths.ConcatDir(dirTemplates, "html")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/html/password-reset.html", "",
		lygo_paths.ConcatDir(dirTemplates, "html")))
	response = append(response, lygo_io.NewAction("",
		repoRoot+"httpoo_templates/html/verified.html", "",
		lygo_paths.ConcatDir(dirTemplates, "html")))

	return response
}
