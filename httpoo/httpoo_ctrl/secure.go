package httpoo_ctrl

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_auth0"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_commons"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppSecure struct {
	mode     string
	dirWork  string // guardian workspace
	logger   *httpoo_commons.Logger
	enabled  bool
	settings *lygo_ext_auth0.Auth0Config
	auth0    *lygo_ext_auth0.Auth0
}

func NewAppSecure(mode string, dirWork string, l *httpoo_commons.Logger) *AppSecure {
	instance := new(AppSecure)
	instance.mode = mode
	instance.dirWork = dirWork
	instance.logger = l
	instance.enabled = false

	instance.init(mode)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppSecure) Enabled() bool {
	if nil != instance {
		return instance.enabled
	}
	return false
}

func (instance *AppSecure) Settings() *lygo_ext_auth0.Auth0Config {
	if nil != instance {
		return instance.settings
	}
	return &lygo_ext_auth0.Auth0Config{
		Secrets:      nil,
		CacheStorage: nil,
		AuthStorage:  &lygo_ext_auth0.Auth0ConfigStorage{
			Driver: "",
			Dsn:    "",
		},
	}
}

func (instance *AppSecure) Auth0() *lygo_ext_auth0.Auth0 {
	if nil != instance && instance.enabled {
		return instance.auth0
	}
	return nil
}

func (instance *AppSecure) Start() error {
	if nil != instance && instance.enabled {
		return instance.auth0.Open()
	}
	return nil
}

func (instance *AppSecure) Stop() {
	if nil != instance && instance.enabled {
		_ = instance.auth0.Close()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppSecure) init(mode string) {
	// search for configuration file
	settings, err := loadSecureSettings(mode)
	if nil == err {
		instance.enabled = true
		instance.settings = settings
		instance.auth0 = lygo_ext_auth0.NewAuth0(settings)
	} else {
		instance.enabled = false
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func loadSecureSettings(mode string) (*lygo_ext_auth0.Auth0Config, error) {
	settings := new(lygo_ext_auth0.Auth0Config)
	path := lygo_paths.WorkspacePath("secure." + mode + ".json")
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	err = lygo_json.Read(text, &settings)
	return settings, err
}
