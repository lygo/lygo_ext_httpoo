package httpoo_ctrl

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_email"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_types"
	"bitbucket.org/lygo/lygo_ext_sms"
	"bitbucket.org/lygo/lygo_ext_sms/engine"
	"errors"
	"fmt"
	"strings"
)

var (
	EmptyTemplateError = errors.New("empty-template")

	extensions = []string{"txt", "htm", "html"}
	dirEmail   = "email"
	dirSms     = "sms"
	dirHtml    = "html"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserverAuthenticationPostman struct {
	config     *httpoo_types.WebServerSettingsAuthenticationPostman
	payload    map[string]interface{}
	apiBaseUrl string
	templates  *AuthenticationTemplates
	sms        *lygo_ext_sms.SMSEngine
	mailHost   string
	mailPort   int
	mailSecure bool
	mailUser   string
	mailPass   string
	mailFrom   string
}

func NewAppWebserverAuthenticationPostman(dirWork, dirTemplates string, config *httpoo_types.WebServerSettingsAuthenticationPostman) *AppWebserverAuthenticationPostman {
	instance := new(AppWebserverAuthenticationPostman)
	instance.config = config
	instance.payload = map[string]interface{}{}

	instance.templates = NewAuthenticationTemplates(dirWork, dirTemplates)

	if nil != config {
		// get payload
		if nil != config.Payload {
			instance.payload = config.Payload
			instance.apiBaseUrl = lygo_reflect.GetString(instance.payload, "api-base-url")
		}
		// initialize SMS channel
		if nil != config.ConfigSms {
			smsConfig, err := engine.NewSMSConfigurationFromMap(config.ConfigSms)
			if nil == err {
				instance.sms = lygo_ext_sms.NewSMSEngine(smsConfig)
			}
		}

		// initialize EMAIL channel
		if nil != config.ConfigMail {
			instance.mailHost = lygo_reflect.GetString(config.ConfigMail, "host")
			instance.mailPort = lygo_reflect.GetInt(config.ConfigMail, "port")
			instance.mailSecure = lygo_reflect.GetBool(config.ConfigMail, "secure")
			instance.mailUser = lygo_reflect.GetString(config.ConfigMail, "user")
			instance.mailPass = lygo_reflect.GetString(config.ConfigMail, "pass")
			instance.mailFrom = lygo_reflect.GetString(config.ConfigMail, "from")
		}
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationPostman) CheckTemplates() ([]string, []error) {
	return instance.templates.Check()
}

func (instance *AppWebserverAuthenticationPostman) GetHtmlTemplate(name string, payloads ...map[string]interface{}) (content string, err error) {
	content, err = instance.content(dirHtml, fmt.Sprintf("%v", name), instance.data(payloads...))
	return
}

// SendEmailTemplate usage: SendEmailTemplate ("verify", "address@info.com", {"user":"Mario"})
func (instance *AppWebserverAuthenticationPostman) SendEmailTemplate(name, to string, payloads ...map[string]interface{}) (err error) {
	var contentHtml, contentText string
	contentHtml, err = instance.content(dirEmail, fmt.Sprintf("%v.html", name), instance.data(payloads...))
	if nil != err {
		return
	}
	contentText, err = instance.content(dirEmail, fmt.Sprintf("%v.text", name), instance.data(payloads...))
	if nil != err {
		return
	}
	subject := instance.subject(dirEmail, name, payloads...)
	err = lygo_email.SendMessage(instance.mailHost, instance.mailPort, instance.mailSecure,
		instance.mailUser, instance.mailPass, instance.mailFrom,
		to, subject, contentText, contentHtml, []interface{}{})

	return
}

func (instance *AppWebserverAuthenticationPostman) SendSmsTemplate(name, to string, payloads ...map[string]interface{}) (response string , err error) {
	var content string
	content, err = instance.content(dirSms, name, instance.data(payloads...))
	if nil != err {
		return
	}

	response, err = instance.sms.SendMessage("", content, to, "")

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationPostman) subject(dir, name string, payloads ...map[string]interface{}) string {
	name = fmt.Sprintf("%v.subject", name)
	content, _ := instance.content(dir, name, payloads...)
	if len(content) == 0 {
		content = "Email"
	}
	return content
}

func (instance *AppWebserverAuthenticationPostman) content(dir, name string, payloads ...map[string]interface{}) (content string, err error) {
	for _, ext := range extensions {
		tplName := fmt.Sprintf("%v.%v", name, ext)
		content, err = instance.templates.Render(dir, tplName, instance.data(payloads...))
		if nil == err {
			break
		}
	}
	if nil != err {
		return
	}

	if len(content) == 0 {
		err = lygo_errors.Prefix(EmptyTemplateError, fmt.Sprintf("%v/%v", dir, name))
	}
	return
}

func (instance *AppWebserverAuthenticationPostman) data(payloads ...map[string]interface{}) map[string]interface{} {
	// creates data with payload
	data := map[string]interface{}{}
	for k, v := range instance.payload {
		data[k] = v
	}
	if len(payloads) > 0 {
		for _, payload := range payloads {
			for k, v := range payload {
				if k == "link" {
					url := lygo_conv.ToString(v)
					if strings.HasPrefix(url, "./") {
						v = lygo_paths.Concat(instance.apiBaseUrl, url)
					} else {
						v = url
					}
				}
				data[k] = v
			}
		}
	}
	return data
}
