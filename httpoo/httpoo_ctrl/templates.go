package httpoo_ctrl

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"github.com/cbroglie/mustache"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AuthenticationTemplates struct {
	dirWork      string
	dirTemplates string
}

func NewAuthenticationTemplates(dirWork, dirTemplates string) *AuthenticationTemplates {
	instance := new(AuthenticationTemplates)
	instance.dirWork = dirWork
	instance.dirTemplates = dirTemplates

	// initialize directories
	if len(instance.dirTemplates) == 0 {
		instance.dirTemplates = "./templates"
	}
	instance.dirTemplates = lygo_paths.Absolutize(instance.dirTemplates, instance.dirWork)
	_ = lygo_paths.Mkdir(instance.dirTemplates + lygo_paths.OS_PATH_SEPARATOR)

	// creates sub dirs
	_ = lygo_paths.Mkdir(lygo_paths.Concat(instance.dirTemplates, "email") + lygo_paths.OS_PATH_SEPARATOR)
	_ = lygo_paths.Mkdir(lygo_paths.Concat(instance.dirTemplates, "sms") + lygo_paths.OS_PATH_SEPARATOR)
	_ = lygo_paths.Mkdir(lygo_paths.Concat(instance.dirTemplates, "html") + lygo_paths.OS_PATH_SEPARATOR)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AuthenticationTemplates) Check() ([]string, []error) {
	// download templates
	return DownloadTemplates(instance.dirTemplates)
}

// Render sample: Render("email", "verify.html", {"username":"Mario"})
func (instance *AuthenticationTemplates) Render(dir, name string, data map[string]interface{}) (string, error) {
	content, err := instance.Get(dir, name)
	if nil != err {
		return "", err
	}
	return mustache.Render(content, data)
}

// Get sample: Get("email", "verify.html")
func (instance *AuthenticationTemplates) Get(dir, name string) (string, error) {
	filename := lygo_paths.Concat(instance.dirTemplates, dir, name)
	return lygo_io.ReadTextFromFile(filename)
}
