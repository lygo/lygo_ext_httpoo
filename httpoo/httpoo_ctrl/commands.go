package httpoo_ctrl

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_commons"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_types"
	"strings"
)

type AppCommands struct {
	defConfig   *httpoo_types.Database
	initialized bool
}

func NewAppCommands(defConfig *httpoo_types.Database) *AppCommands {
	instance := new(AppCommands)
	instance.defConfig = defConfig

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppCommands) Execute(filename string, params interface{}, dbConfig *httpoo_types.Database) (interface{}, error) {
	ext := strings.ToLower(lygo_paths.ExtensionName(filename))
	switch ext {
	case "sh":
		args := lygo_conv.ToArrayOfString(params)
		return lygo_exec.RunOutput(filename, args...)
	case "aql":
		return instance.executeAQL(filename, params, dbConfig)
	default:
		return nil, httpoo_commons.CommandUnsupported
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppCommands) database(dbConfig *httpoo_types.Database) (dbal_drivers.IDatabase, error){
	if nil == dbConfig || len(dbConfig.Driver)==0{
		dbConfig = instance.defConfig
	}
	return dbal_drivers.NewDatabase(dbConfig.Driver, dbConfig.Dsn)
}

func (instance *AppCommands) executeAQL(filename string, params interface{}, dbConfig *httpoo_types.Database) (interface{}, error) {
	db, err := instance.database(dbConfig)
	if nil != err {
		return nil, err
	}
	query, err := lygo_io.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}

	return db.ExecNative(query, lygo_conv.ForceMap(params))
}
