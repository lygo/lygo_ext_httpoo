package httpoo_ctrl

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_http/httpserver"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_types"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"os"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type Webserver struct {
	name        string
	mode        string
	dirWork     string // workspace
	initialized bool
	enabled     bool

	settings       *httpoo_types.WebServerSettings
	httpserver     *httpserver.HttpServer
	httpRoot       string
	httpStaticRoot string
	httpAddr       string
	httpsAddr      string
}

func NewWebserver(name, mode string, dirWork string) *Webserver {
	instance := new(Webserver)
	instance.name = name
	instance.mode = mode
	instance.dirWork = dirWork
	instance.enabled = false

	_ = instance.init(name, mode)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Webserver) HttpRoot() string {
	if nil != instance {
		return instance.httpRoot
	}
	return ""
}

func (instance *Webserver) HttpPath(path string) string {
	if nil != instance {
		return lygo_paths.Concat(instance.httpRoot, path)
	}
	return ""
}

func (instance *Webserver) HttpStaticRoot() string {
	if nil != instance {
		return instance.httpStaticRoot
	}
	return ""
}

func (instance *Webserver) HttpAddress() string {
	if nil != instance {
		return instance.httpAddr
	}
	return ""
}

func (instance *Webserver) LocalUrl() string {
	if nil != instance && instance.enabled {
		if len(instance.httpAddr) > 0 {
			return fmt.Sprintf("http://localhost%v/", instance.httpAddr)
		} else if len(instance.httpsAddr) > 0 {
			return fmt.Sprintf("https://localhost%v/", instance.httpsAddr)
		}
	}
	return ""
}

func (instance *Webserver) Settings() *httpoo_types.WebServerSettings {
	if nil != instance {
		return instance.settings
	}
	return nil
}

func (instance *Webserver) Initialize(handlers []httpoo_types.IRouteHandler) error {
	err := instance.init(instance.name, instance.mode)
	if nil == err {
		if nil != handlers {
			for _, handler := range handlers {
				endpoint := handler.Endpoint()
				method := handler.Method()
				switch strings.ToLower(method) {
				case "get":
					instance.httpserver.Get(endpoint.String(), handler.Handle)
				case "post":
					instance.httpserver.Post(endpoint.String(), handler.Handle)
				case "put":
					instance.httpserver.Put(endpoint.String(), handler.Handle)
				case "delete":
					instance.httpserver.Delete(endpoint.String(), handler.Handle)
				case "middleware":
					instance.httpserver.Middleware(endpoint.String(), handler.Handle)
				default:
					// ALL
					instance.httpserver.All(endpoint.String(), handler.Handle)
				}
			}
		}
	}
	return err
}

func (instance *Webserver) Handle(method, endpoint string, handler fiber.Handler) {
	switch strings.ToLower(method) {
	case "get":
		instance.httpserver.Get(endpoint, handler)
	case "post":
		instance.httpserver.Post(endpoint, handler)
	case "put":
		instance.httpserver.Put(endpoint, handler)
	case "delete":
		instance.httpserver.Delete(endpoint, handler)
	case "middleware":
		instance.httpserver.Middleware(endpoint, handler)
	default:
		instance.httpserver.All(endpoint, handler)
	}
}

func (instance *Webserver) IsEnabled() bool {
	if nil != instance {
		return instance.enabled
	}
	return false
}

func (instance *Webserver) Start() bool {
	if nil != instance && instance.enabled {
		instance.start()

		return true
	}
	return false
}

func (instance *Webserver) Stop() {
	if nil != instance && instance.enabled {
		_ = instance.httpserver.Stop()
	}
}

func (instance *Webserver) Exit() {
	defer func() {
		if r := recover(); r != nil {
			// recover from panic if any
		}
	}()
	if nil != instance && instance.enabled {
		go func() {
			_ = instance.httpserver.Stop()
		}()
		// wait a while the server close
		time.Sleep(3 * time.Second)
		if instance.httpserver.IsOpen() {
			// brute force close
			os.Exit(0)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Webserver) init(name, mode string) (err error) {
	if !instance.initialized {
		instance.initialized = true

		// search for configuration file
		var settings *httpoo_types.WebServerSettings
		settings, err = loadSettings(name, mode)
		if nil == err {
			instance.settings = settings
			instance.enabled = settings.Enabled
			if instance.enabled {

				// create webserver
				err := instance.initHttp()
				if nil != err {
					instance.enabled = false
					return err
				}

				// parse settings
				http := instance.settings.Http
				hosts := lygo_reflect.GetArray(http, "hosts")
				for _, host := range hosts {
					isSecure := lygo_reflect.GetBool(host, "tls")
					if isSecure {
						instance.httpsAddr = lygo_reflect.GetString(host, "addr")
					} else {
						instance.httpAddr = lygo_reflect.GetString(host, "addr")
					}
				}
				static := lygo_reflect.GetArray(http, "static")
				if len(static) > 0 {
					root := lygo_reflect.GetString(static[0], "root")
					instance.httpStaticRoot = lygo_paths.Concat(instance.httpRoot, root)
				}

			}
		} else {
			instance.enabled = false
		}
	}
	return
}

func (instance *Webserver) initHttp() error {
	// create webserver instance
	httpConfig := instance.settings.Http
	instance.httpRoot = lygo_reflect.GetString(httpConfig, "root")
	if len(instance.httpRoot) == 0 {
		instance.httpRoot = instance.dirWork
	} else {
		if !lygo_paths.IsAbs(instance.httpRoot) {
			instance.httpRoot = lygo_paths.Concat(instance.dirWork, instance.httpRoot)
		}
	}

	instance.httpserver = httpserver.NewHttpServer(instance.httpRoot, instance.handleHttError, instance.handleHttpLimit)
	err := instance.httpserver.ConfigureFromMap(httpConfig)
	if nil != err {
		return err
	}
	// check defaults
	configuration := instance.httpserver.Configuration()
	if nil != configuration {
		if nil != configuration.Server {
			if configuration.Server.ReadTimeout < 3*time.Second {
				configuration.Server.ReadTimeout = 3 * time.Second
			}
			if configuration.Server.WriteTimeout < 3*time.Second {
				configuration.Server.WriteTimeout = 3 * time.Second
			}
		}
	}

	return nil
}

func (instance *Webserver) start() {
	// wait a while before start to allow runtime is ready
	time.Sleep(2 * time.Second)
	instance.httpserver.Start()
}

func (instance *Webserver) handleHttError(serverError *httpserver.HttpServerError) {

}

func (instance *Webserver) handleHttpLimit(ctx *fiber.Ctx) error {
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func loadSettings(name, mode string) (*httpoo_types.WebServerSettings, error) {
	path := lygo_paths.WorkspacePath(name + "." + mode + ".json")
	settings := new(httpoo_types.WebServerSettings)
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	err = lygo_json.Read(text, &settings)
	return settings, err
}
