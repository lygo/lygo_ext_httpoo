package httpoo_commons

import (
	_ "embed"
	"errors"
)

const (
	AppName    = "HTTPoo"
	AppVersion = "0.0.1"

	ModeProduction = "production"
	ModeDebug      = "debug"

	EventOnDoStop = "on_do_stop"
)

//-- workspaces --//
const (
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError = errors.New("panic_system_error")

	EndpointNotReadyError = errors.New("endpoint_not_ready")

	MissingConfigurationError = errors.New("missing_configuration")

	CommandUnsupported     = errors.New("command_unsupported")
	CommandMissingParamError         = errors.New("command_missing_param")

	HttpUnauthorizedError       = errors.New("unauthorized")          // 401
	HttpInvalidCredentialsError = errors.New("invalid_credentials")   // 401
	AccessTokenExpiredError     = errors.New("access_token_expired")  // 403
	RefreshTokenExpiredError    = errors.New("refresh_token_expired") // 401
	AccessTokenInvalidError     = errors.New("access_token_invalid")  // 401
	HttpUnsupportedApiError     = errors.New("unsupported_api")
)

// ---------------------------------------------------------------------------------------------------------------------
//		e m b e d d e d
// ---------------------------------------------------------------------------------------------------------------------

//go:embed tpl_webserver.json
var TplFileWebserver string

//go:embed tpl_secure.json
var TplFileSecure string
