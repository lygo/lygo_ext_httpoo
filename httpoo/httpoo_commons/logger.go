package httpoo_commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_logs"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Logger struct {
	mode   string
	root   string
	logger *lygo_ext_logs.Logger
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewLogger(mode string, logger interface{}) *Logger {
	instance := new(Logger)
	instance.mode = mode
	instance.root = lygo_paths.WorkspacePath("logging")

	// reset file
	_ = lygo_io.RemoveAll(instance.root)
	_ = lygo_paths.Mkdir(instance.root + lygo_paths.OS_PATH_SEPARATOR)

	if instance.logger, _ = logger.(*lygo_ext_logs.Logger); nil == instance.logger {
		instance.logger = lygo_ext_logs.NewLogger()
		instance.logger.SetFileName(lygo_paths.Concat(instance.root, "logging.log"))
	}

	if mode == ModeDebug {
		instance.logger.SetLevel(lygo_ext_logs.LEVEL_DEBUG)
	} else {
		instance.logger.SetLevel(lygo_ext_logs.LEVEL_INFO)
	}
	instance.logger.SetOutput(lygo_ext_logs.OUTPUT_FILE)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) Close() {
	instance.logger.Close()
}

func (instance *Logger) SetLevel(level string) {
	instance.logger.SetLevelName(level)
}

func (instance *Logger) GetLevel() int {
	return instance.logger.GetLevel()
}

func (instance *Logger) Debug(args ...interface{}) {
	// file logging
	instance.logger.Debug(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Info(args ...interface{}) {
	// file logging
	instance.logger.Info(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Error(args ...interface{}) {
	// file logging
	instance.logger.Error(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Warn(args ...interface{}) {
	// file logging
	instance.logger.Warn(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}
