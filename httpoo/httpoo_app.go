package httpoo

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_commons"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_ctrl"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_types"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserver struct {
	mode    string
	dirWork string //  workspace

	handlers      []httpoo_types.IRouteHandler
	logger        *httpoo_commons.Logger
	secureManager *httpoo_ctrl.AppSecure

	webserver               *httpoo_ctrl.Webserver
	webserverAuthentication *AppWebserverAuthentication
	commandExecutor         *httpoo_ctrl.AppCommands
}

func NewAppWebserver(mode string, customLogger interface{}) *AppWebserver {
	instance := new(AppWebserver)
	instance.mode = mode
	instance.dirWork = lygo_paths.GetWorkspacePath()
	instance.logger = httpoo_commons.NewLogger(mode, customLogger)
	instance.handlers = make([]httpoo_types.IRouteHandler, 0)

	// initialize configuration
	instance.initConfiguration()

	instance.secureManager = httpoo_ctrl.NewAppSecure(mode, instance.dirWork, instance.logger)
	instance.webserver = httpoo_ctrl.NewWebserver("webserver", mode, instance.dirWork)

	instance.commandExecutor = httpoo_ctrl.NewAppCommands(&httpoo_types.Database{
		Driver: instance.secureManager.Settings().AuthStorage.Driver,
		Dsn:    instance.secureManager.Settings().AuthStorage.Dsn,
	})

	_ = instance.initHandlers()
	_ = instance.initAuthenticationHandlers()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserver) Start() (err error) {
	if nil != instance && nil != instance.webserver {
		err = instance.webserver.Initialize(instance.handlers)
		if nil == err {
			err = instance.secureManager.Start()
			if nil == err {
				if instance.webserver.Start() {
					// SUCCESS
				} else {
					err = httpoo_commons.MissingConfigurationError
				}
			}
		}
	}
	return
}

func (instance *AppWebserver) Stop() {
	if nil != instance && nil != instance.webserver {
		instance.secureManager.Stop()
		instance.webserver.Stop()
	}
}

func (instance *AppWebserver) Exit() {
	if nil != instance && nil != instance.webserver {
		instance.secureManager.Stop()
		instance.webserver.Exit()
	}
}

func (instance *AppWebserver) HttpPath(path string) string {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.HttpPath(path)
	}
	return ""
}

func (instance *AppWebserver) LocalUrl() string {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.LocalUrl()
	}
	return ""
}

func (instance *AppWebserver) Handle(method, endpoint string, auth *httpoo_types.Authorization, handler HandlerCallback) {
	if nil != instance && nil != instance.webserver {
		route := &httpoo_types.WebServerSettingsRoute{
			Method:        strings.ToLower(method),
			Endpoint:      endpoint,
			Authorization: auth,
		}
		instance.HandleRoute(route, handler)
	}
}

func (instance *AppWebserver) HandleRoute(route *httpoo_types.WebServerSettingsRoute, handler HandlerCallback) {
	if nil != instance && nil != instance.webserver && nil != route {
		rh := NewRouteHandler(instance.secureManager, route, handler)
		instance.handlers = append(instance.handlers, rh)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
func (instance *AppWebserver) initConfiguration() {
	_ = lygo_paths.Mkdir(instance.dirWork)

	// secure
	secureConfig := lygo_paths.Concat(instance.dirWork, fmt.Sprintf("secure.%s.json", instance.mode))
	if b, _ := lygo_paths.Exists(secureConfig); !b {
		_, _ = lygo_io.WriteTextToFile(httpoo_commons.TplFileSecure, secureConfig)
	}

	// webserver
	webserverConfig := lygo_paths.Concat(instance.dirWork, fmt.Sprintf("webserver.%s.json", instance.mode))
	if b, _ := lygo_paths.Exists(webserverConfig); !b {
		_, _ = lygo_io.WriteTextToFile(httpoo_commons.TplFileWebserver, webserverConfig)
	}

}

func (instance *AppWebserver) initHandlers() error {
	if nil != instance && nil != instance.webserver && instance.webserver.IsEnabled() && nil != instance.webserver.Settings() {
		routing := instance.webserver.Settings().Routing
		authentication := instance.webserver.Settings().Authentication
		for _, route := range routing {
			if nil == route.Authorization {
				if nil != authentication && nil != authentication.Security {
					route.Authorization = authentication.Security.Authorization
				} else {
					// free-access
					route.Authorization = &httpoo_types.Authorization{
						Type:  "none",
						Value: "",
					} // replace with defaults
				}
			}
			handler := NewRouteHandler(instance.secureManager, route, instance.onRoute)
			instance.handlers = append(instance.handlers, handler)
		}
		return instance.webserver.Initialize(instance.handlers)
	}
	return nil
}

func (instance *AppWebserver) initAuthenticationHandlers() error {
	if nil != instance && nil != instance.webserver && instance.webserver.IsEnabled() && nil != instance.webserver.Settings() {
		authenticationSettings := instance.webserver.Settings().Authentication
		if nil != authenticationSettings {
			instance.webserverAuthentication = NewAppWebserverAuthentication(instance.mode,
				instance.dirWork, instance.webserver.HttpRoot(), instance.logger, authenticationSettings, instance.webserver, instance.secureManager)
			// initialize templates and authentication engine
			files, errs := instance.webserverAuthentication.Start()
			if len(errs) > 0 {
				instance.logger.Warn("Authentication Templates are not enabled: ", errs)
			} else {
				if len(files) > 0 {
					instance.logger.Debug(fmt.Sprintf("Authentication Templates downloaded %v new files.", len(files)))
				} else {
					instance.logger.Debug("Authentication Templates are enabled.")
				}
			}
		}
	}
	return nil
}

// onRoute executes commands for OS. Sample endpoint: /api/v1/commands/command1.sh
func (instance *AppWebserver) onRoute(ctx *fiber.Ctx, handler *RouteHandler) error {
	if nil != instance && nil != instance.webserver && instance.webserver.IsEnabled() {
		// handle a command to execute
		dir := instance.HttpPath(handler.endpoint.Group)
		if b, _ := lygo_paths.Exists(dir); b {
			filename := lygo_paths.Concat(dir, handler.endpoint.Name)
			params := lygo_reflect.Get(Params(ctx, true), "params")
			if nil == params {
				params = Params(ctx, true)
			}
			out, err := instance.commandExecutor.Execute(filename, params, handler.database)

			return WriteResponse(ctx, out, err)
		}
		return httpoo_commons.HttpUnsupportedApiError
	}
	return httpoo_commons.PanicSystemError
}
