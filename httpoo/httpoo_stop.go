package httpoo

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_commons"
	"sync"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type stopMonitor struct {
	roots      []string // where stop file is stored
	stopCmd    string
	events     *lygo_events.Emitter
	fileMux    sync.Mutex
	stopTicker *lygo_events.EventTicker
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func newStopMonitor(roots []string, stopCmd string, events *lygo_events.Emitter) *stopMonitor {
	instance := new(stopMonitor)
	instance.roots = roots
	instance.stopCmd = stopCmd
	instance.events = events

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *stopMonitor) Start() {
	if nil != instance && len(instance.stopCmd) > 0 && nil == instance.stopTicker {
		instance.stopTicker = lygo_events.NewEventTicker(1*time.Second, func(t *lygo_events.EventTicker) {
			instance.checkStop()
			// instance.logger.Debug("Checking for stop command....")
		})
		instance.stopTicker.Start()
	}
}

func (instance *stopMonitor) Stop() {
	if nil != instance && nil != instance.stopTicker {
		instance.stopTicker.Stop()
		instance.stopTicker = nil
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *stopMonitor) checkStop() {
	if nil != instance && len(instance.stopCmd) > 0 {
		instance.fileMux.Lock()
		defer instance.fileMux.Unlock()

		cmd := instance.stopCmd

		// check if file exists
		for _, root := range instance.roots {
			cmdFile := lygo_paths.Concat(root, cmd)
			if b, _ := lygo_paths.Exists(cmdFile); b {
				_ = lygo_io.Remove(cmdFile)
				instance.events.EmitAsync(httpoo_commons.EventOnDoStop)
			}
		}
	}
}
